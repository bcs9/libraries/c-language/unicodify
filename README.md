# BCS Unicodify

A simple single header C library for converting utf8 strings to unicode and back.

## Can I use this?

Absolutely!

Just please do not remove the licenses from the files.

If that bothers you, then please, don't use this program.

## Dependencies

1. C Standard Library

## Example Compilation

`make all`

Or for including the address sanitizer for debugging purposes:

`make debug`