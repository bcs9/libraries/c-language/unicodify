/*
Copyright © 2021 Bradford C. Shapleigh

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>

#include "../BCS_unicodify.h"

/* Main */
int main() {
    char* test_string = "Welcome to Japan!\n日本へようこそ！";
    unsigned long long test_string_length = strlen(test_string);
    BCS_unicodify__et error = BCS_unicodify__et_no_error;
    char* final_string = 0;

    // deserialize code points
    BCS_unicodify__code_points code_points = BCS_unicodify__deserialize_utf8(&error, test_string, test_string_length);
    if (error != BCS_unicodify__et_no_error) {
        goto BCS_unicodify__label_destroy_code_points;
    }

    // print code points
    for (unsigned long long i = 0; i < code_points.p_length; i++) {
        printf("%llu: %i\n", i, code_points.p_values[i]);
    }
    fflush(stdout);

    // reserialize the code points
    final_string = BCS_unicodify__serialize_utf8(&error, code_points);
    if (error != BCS_unicodify__et_no_error) {
        goto BCS_unicodify__label_destroy_code_points;
    }

    // check final result
    printf("Deserialized: %s\n", final_string);
    fflush(stdout);

    // clean up
    free(final_string);
    BCS_unicodify__label_destroy_code_points: BCS_unicodify__destroy_code_points(&code_points);

    if (error != BCS_unicodify__et_no_error) {
        printf("Error: Main Error: %i\n", error);
        fflush(stdout);

        return 1;
    }

    // fin!
    return 0;
}