/*
Copyright © 2021 Bradford C. Shapleigh

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __BCS_UNICODIFY__
#define __BCS_UNICODIFY__

#include <stdlib.h>

/* Types */
typedef enum BCS_unicodify__et {
    // none
    BCS_unicodify__et_no_error,
    
    // extracting
    BCS_unicodify__et_invalid_utf8,

    // deserializing
    BCS_unicodify__et_invalid_code_points,
} BCS_unicodify__et;

typedef struct BCS_unicodify__code_points {
    unsigned long long p_length;
    int* p_values;
} BCS_unicodify__code_points;

/* Destroying Code Points */
void BCS_unicodify__destroy_code_points(BCS_unicodify__code_points* code_points) {
    free(code_points->p_values);

    return;
}

/* Deserializing Code Points */
int BCS_unicodify__get_code_point_length_from_string(unsigned char *str, long long index) {
    if ((str[index] & 0b11111000) == 0b11110000) {
        return 4;
    } else if ((str[index] & 0b11110000) == 0b11100000) {
        return 3;
    } else if ((str[index] & 0b11100000) == 0b11000000) {
        return 2;
    } else if (str[index] < 128) {
        return 1;
    } else {
        return 0;
    }
}

int BCS_unicodify__get_code_point_value(unsigned char *str, long long index, long long *length) {
    int value = 0;

    if ((str[index] & 0b11111000) == 0b11110000) {
        value += ((int)(str[index] & 0b00000111)) << 18;
        value += ((int)(str[index + 1] & 0b00111111)) << 12;
        value += ((int)(str[index + 2] & 0b00111111)) << 6;
        value += (int)(str[index + 3] & 0b00111111);

        *length = 4;
    } else if ((str[index] & 0b11110000) == 0b11100000) {
        value += ((int)(str[index] & 0b00001111)) << 12;
        value += ((int)(str[index + 1] & 0b00111111)) << 6;
        value += (int)(str[index + 2] & 0b00111111);

        *length = 3;
    } else if ((str[index] & 0b11100000) == 0b11000000) {
        value += ((int)(str[index] & 0b00011111)) << 6;
        value += (int)(str[index + 1] & 0b00111111);

        *length = 2;
    } else if (str[index] < 128) {
        value = str[index];

        *length = 1;
    }

    return value;
}

BCS_unicodify__code_points BCS_unicodify__deserialize_utf8(BCS_unicodify__et* error, char* str, unsigned long long str_length) {
    BCS_unicodify__code_points output;
    unsigned long long length = 0;
    unsigned long long index = 0;
    unsigned long long current_point = 0;
        
    // initialize the output count, but not the array
    output.p_length = 0;

    // get the amount of the code points
    while (index < str_length) {
        length = BCS_unicodify__get_code_point_length_from_string(str, index);

        if (length == 0) {
            *error = BCS_unicodify__et_invalid_utf8;

            return output;
        }

        index += length;
        output.p_length++;
    }

    // for null termination of code points
    output.p_length++;

    // initialize the code points
    output.p_values = (int*)malloc(sizeof(int) * output.p_length);

    // reset index counter
    index = 0;

    // get code points
    while (current_point < output.p_length - 1) {
        output.p_values[current_point] = BCS_unicodify__get_code_point_value(str, index, &length);
        index += length;
        current_point++;
    }

    // set null termination for code points
    output.p_values[output.p_length - 1] = 0;

    return output;
}

/* Serializing Code Points */
unsigned char BCS_unicodify__get_code_point_length_from_code_point(int code_point) {
    if (code_point >= (1 << 19)) {
        return 4;
    } else if (code_point >= (1 << 13)) {
        return 3;
    } else if (code_point >= (1 << 7)) {
        return 2;
    } else if (code_point < 128) {
        return 1;
    } else {
        return 0;
    }
}

unsigned long long BCS_unicodify__write_utf8_character_to_string(char* output, unsigned long long index, int code_point) {
    if (code_point >= (1 << 19)) {
        output[index] = (0b00000111 & (unsigned char)(code_point >> 18)) + 0b11110000;
        output[index + 1] = (0b00111111 & (unsigned char)(code_point >> 12)) + 0b10000000;
        output[index + 2] = (0b00111111 & (unsigned char)(code_point >> 6)) + 0b10000000;
        output[index + 3] = (0b00111111 & (unsigned char)code_point) + 0b10000000;

        return 4;
    } else if (code_point >= (1 << 13)) {
        output[index] = (0b00001111 & (unsigned char)(code_point >> 12)) + 0b11100000;
        output[index + 1] = (0b00111111 & (unsigned char)(code_point >> 6)) + 0b10000000;
        output[index + 2] = (0b00111111 & (unsigned char)code_point) + 0b10000000;

        return 3;
    } else if (code_point >= (1 << 7)) {
        output[index] = (0b00011111 & (unsigned char)(code_point >> 6)) + 0b11000000;
        output[index + 1] = (0b00111111 & (unsigned char)code_point) + 0b10000000;

        return 2;
    } else if (code_point < 128) {
        output[index] = (unsigned char)(0b01111111 & (unsigned char)code_point);

        return 1;
    } else {
        return 0;
    }
}

char* BCS_unicodify__serialize_utf8(BCS_unicodify__et* error, BCS_unicodify__code_points code_points) {
    char* output;
    unsigned long long current_code_point = 0;
    unsigned long long output_index = 0;
    unsigned long long code_point_length;

    // get the amount of code points
    while (current_code_point < code_points.p_length) {
        code_point_length = BCS_unicodify__get_code_point_length_from_code_point(code_points.p_values[current_code_point]);
        
        if (code_point_length == 0) {
            *error = BCS_unicodify__et_invalid_code_points;

            return 0;
        }

        output_index += code_point_length;
        current_code_point++;
    }

    // initialize the utf8 string
    output = (char*)malloc(output_index);

    // reset counters
    current_code_point = 0;
    output_index = 0;

    // create the deserialized string
    while (current_code_point < code_points.p_length) {
        output_index += BCS_unicodify__write_utf8_character_to_string(output, output_index, code_points.p_values[current_code_point]);
        current_code_point++;
    }

    return output;
}

#endif